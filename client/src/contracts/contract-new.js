import React from "react"
import ROUTES from "../routes"

import svc from "./contracts-service"
import customerService from "./../customers/customers-service"
import ContractForm from "./contract-form"
class ContractNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: {
        name: "",
        price: "",
        description:""
      },
      customers: [],
      statuses: []
    }
  }
  componentDidMount() {
    this.setState(this.props.contract)
  }
  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  async load() {
    let res = await customerService.getCustomers()
    this.setState({
      customers: res.data
    })

    res = await svc.getStatuses()
    this.setState({
      statuses: res.data
    })
  }

  create() {
     if (!this.state.contract.customerId) { 
       alert("Choose cutomer please.")
       return
      }

    svc.createContract(this.state.contract).then(res => {
      this.props.history.push(ROUTES.CONTRACT_LISTING)
    })
  }

  handleChange(e) {
    this.setState({
      ...this.state,
      contract: {
        ...this.state.contract,
        [e.target.name]: e.target.value
      }
    })
  }

  render() {
    const contract = this.state.contract
    const customers = this.state.customers
    const statuses = this.state.statuses

    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="box">
            <div className="box-header">
            <h3 className="box-title">New Contract</h3>
            </div>
            <div className="box-body">
              <ContractForm
                contract={contract}
                customers={customers}
                statuses={statuses}
                handleChange={e => this.handleChange(e)}
              />
            </div>
            <div className="box-footer">
              {<button
                  className="btn btn-primary"
                  onClick={() => this.create()}
                >
                  Create
                </button>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ContractNew
