import React from "react"
import { Link } from "react-router-dom"
import ROUTES from "../routes"

import svc from "./contracts-service"
import { price, dateToLocal } from "../format"
import authService from "../auth-service"

class ContractDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: {},
      customer: [],
      comments: [],
      newCommentText: ""
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
    this.loadComments(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
      this.loadComments(newProps.match.params.id)
    }
  }

  async load(id) {
    let res = await svc.getContract(id)

    this.setState({
      contract: res.data,
      customer: res.data.customer
    })
  }

  async loadComments(id) {
    let res = await svc.getContractComments(id)

    this.setState({
      comments: res.data
    })
  }

  create() {
    svc.createContract(this.state.contract).then(res => {
      this.props.history.push(ROUTES.CONTRACT_LISTING)
    })
  }

  createComment() {
    svc
      .createContractComment(this.state.contract.id, {
        id: "",
        text: this.state.newCommentText
      })
      .then(res => {
        this.setState({ comments: this.state.comments.concat(res.data) })
      })
    this.setState({ newCommentText: "" })
  }

  updateNewComment(e) {
    this.setState({ newCommentText: e.target.value })
  }

  render() {
    const contract = this.state.contract
    const customer = this.state.customer

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-8">
            <h2>Detail</h2>

            <div className="btn-group">
              <Link
                className="btn btn-light"
                to={ROUTES.getUrl(ROUTES.CONTRACT_EDIT, { id: contract.id })}
              >
                Edit
              </Link>
              <Link
                className="btn btn-danger"
                to={ROUTES.getUrl(ROUTES.CONTRACT_LISTING)}
              >
                Delete
              </Link>
            </div>

            <table>
              <tbody>
                <tr>
                  <th>Name</th>
                  <td>{contract.name}</td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td>{contract.description}</td>
                </tr>
                <tr>
                  <th>Price</th>
                  <td>{price(contract.price)}</td>
                </tr>
                <tr>
                  <th>Customer </th>
                  <td>
                    <Link
                      to={ROUTES.getUrl(ROUTES.CUSTOMER_DETAIL, {
                        id: contract.customerId
                      })}
                    >
                      {customer.name}
                    </Link>
                  </td>
                </tr>
                <tr>
                  <th>Created</th>
                  <td>{dateToLocal(contract.dateCreated)}</td>
                </tr>
                <tr>
                  <th>Edited</th>
                  <td>
                    {contract.dateEdited
                      ? dateToLocal(contract.dateEdited)
                      : ""}
                  </td>
                </tr>
                <tr>
                  <th>Deadline</th>
                  <td>
                    {contract.deadline ? dateToLocal(contract.deadline) : ""}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="col-4">
            <div className="commentsBox">
              <div className="titleBox">
                <label>Comments</label>
              </div>
              <ul className="list-group comments">
                {this.state.comments.map(c => (
                  <li
                    className="list-group-item justify-content-between"
                    style={{ border: "none" }}
                    key={c.id}
                  >
                    <small className="text-muted">
                      {new Date(c.date).toLocaleDateString("sv-se") + " "}
                    </small>
                    {c.text}
                    <p className="text-right">{authService.user.username}</p>
                  </li>
                ))}
              </ul>
              <form className="form-inline comments">
                <div className="form-group">
                  <input
                    className="form-control"
                    placeholder="New comment"
                    value={this.state.newCommentText}
                    onChange={e => this.updateNewComment(e)}
                  />
                  <button
                    type="button"
                    className="btn btn-sm btn-default ml-2"
                    onClick={() => this.createComment()}
                  >
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ContractDetail
