import http from '../http'
import _ from 'lodash'

export default {
  getVersion() {
    return http.get('/version').then(_.property('data'))
  }
}
