import React from 'react';
import moment from 'moment';

import { Select } from '../components/Select';

import auditLogService from './audit-log-service';

const ENTITY = [
  { name: 'NULL', label: 'No constraint', ops: ['NULL', 'create', 'update', 'delete', 'login'] },
  { name: 'users', label: 'Users', ops: ['NULL', 'create', 'update', 'delete'] },
  { name: 'customers', label: 'Customers', ops: ['NULL', 'create', 'update', 'delete'] },
  { name: 'contracts', label: 'Contracts', ops: ['NULL', 'create', 'update', 'delete'] },
  { name: 'comments', label: 'Comments', ops: ['NULL', 'create', 'update'] },
  { name: 'authentication', label: 'Authentication', ops: ['NULL', 'login']},
];

const OPERATION = [
  { name: 'NULL', label: 'No constraint' },
  { name: 'create', label: 'Create' },
  { name: 'update', label: 'Update' },
  { name: 'delete', label: 'Delete' },
  { name: 'login', label: 'Log in' },
];

function entityByName(name) {
  return ENTITY.find(o => o.name === name)
}

function operationByName(name) {
  return OPERATION.find(o => o.name === name)
}

export default class AuditLog extends React.Component {

  componentDidMount() {
    this.queryAuditLogService({});
  }

  state = {
    entity: 'NULL',
    operation: 'NULL',
    results: [],
  }

  async queryAuditLogService({entity, operation}) {
    let result = await auditLogService.query({
      entity: entity !== 'NULL' ? entity : undefined, 
      operation: operation !== 'NULL' ? operation : undefined,
    });
    this.setState({
      results: result.data || []
    });
  }

  setEntity(value) {
    this.setState(state => {
      const newState = {
        ...state,
        entity: value,
        // When operation is impossible to keep for a newly selected entity, choose the first possible operation
        operation: entityByName(value).ops.indexOf(state.operation) > -1 ? state.operation : entityByName(value).ops[0],
      };
      this.queryAuditLogService(newState);
      return newState;
    });
  }

  setOperation(value) {
    this.setState(state => {
      const newState = {
        ...state,
        operation: value,
      };
      this.queryAuditLogService(newState);
      return newState;
    });
  }

  getOperations() {
    const ce = entityByName(this.state.entity);
    return OPERATION.filter(o => ce.ops.indexOf(o.name) > -1);
  }

  getAuditMessage(ar) {
    let message = '';
    switch(ar.operation) {
      case 'login':
        message = `User has logged in.`;
        break;
      case 'create':
        message = `${entityByName(ar.entity).label}: Record created.`;
        break;
      case `update`:
        message = `${entityByName(ar.entity).label}: Record updated.`;
        break;
      case `delete`:
        message = `${entityByName(ar.entity).label}: Record deleted.`;
        break;           
      default:
        message = `Unknown operation`;   
        break;
    }
    return message;
  }


  

  render() {
    return (
      <div className="box">
        <div className="box-header">
          <h3 className="box-title">Audit log</h3>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label className="col-form-label">Entity</label>
              <Select className="form-control" items={ENTITY} onChange={value => this.setEntity(value)} />
            </div>
            <div className="form-group col-md-4">
              <label className="col-form-label">Operation</label>
              <Select className="form-control" items={this.getOperations()} onChange={value => this.setOperation(value)} />
            </div>
          </div>
        </div>
        <div className="box-body">
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Time</th>
                <th>User</th>
                <th>Message</th>
              </tr>
            </thead>
            <tbody>
              {this.state.results.map(record =>
                <tr key={record.id}>
                  <td>{moment(record.created_at).format('D.M.YYYY \u2013 HH:mm')}</td>
                  <td>{record.user}</td>
                  <td>{this.getAuditMessage(record)}</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}