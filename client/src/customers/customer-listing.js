import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import customersService from './customers-service'
import _ from 'lodash'

class CustomerListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customers: [],
      searchText: ''
    }

    this.handleChange = this.handleChange.bind(this);

    this.loadDebounced = _.debounce(this.load, 200) // 1000 to feel strongly the difference
  }

  componentWillMount() {
    this.load(this.state.searchText)
  }

  async load() {
    let res = await customersService.searchCustomers(this.state.searchText)

    this.setState({
      customers: res.data
    })
  }

  handleChange(event) {
    this.setState({ searchText: event.target.value });

    this.loadDebounced(this.state.searchText);
  }

  render() {
    const customers = this.state.customers

    return (
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers</h3>
              <div class="box-tools">
                <form onSubmit={this.handleSubmit}>
                  <label>
                    <input 
                      name="q"
                      type="text"
                      className="form-control input-sm"
                      placeholder="Search customers" 
                      value={this.state.searchText} 
                      onChange={this.handleChange} 
                    />
                  </label>
                </form>
              </div>
            </div>
            <div class="box-body">
              <div className="row">
                <div className="col-sm-12">
                  <table className="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Note</th>
                      </tr>  
                    </thead>    
                    <tbody>
                      {customers.map(c =>
                        <tr key={c.id}>
                          <td>
                            <Link to={ROUTES.getUrl(ROUTES.CUSTOMER_DETAIL, { id: c.id })}>{c.name}</Link>
                          </td>
                          <td>{c.email}</td>
                          <td>{c.phone}</td>
                          <td>{c.city}</td>
                          <td>{('' + c.note).slice(0, 60)}</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-5">
                  <Link to={ROUTES.CUSTOMER_NEW} className="btn btn-default">Create new</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CustomerListing
