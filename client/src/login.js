import React from 'react'
import versionService from './version/version-service'
import authService from './auth-service'
import {getIn, setIn} from './utilities/common';
import {Messages} from './utilities/components';

const PURPOSE_LOGIN = 0;
const PURPOSE_RECOVER_PASSWORD = 1;

class Login extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      credentials: {
        "email": "",
        "password": ""
      },
      dialogPurpose: PURPOSE_LOGIN,
      errorMessage: undefined,
      infoMessage: undefined,
    }
  }

  async loadVersion() {
    this.setState({
      version: await versionService.getVersion()
    })
  }

  componentWillMount() {
    this.loadVersion()
  }
   
  withShowError(promise) {
    return promise.catch(error => {
      this.setState({
        errorMessage: getIn(error, ['response', 'data', 'status'], 'Something went wrong :('),
      });
    })
  }  

  runWithShowError(fcn) {
    return this.withShowError(fcn())
  }

  dismissError = (event) => {
    this.setState({errorMessage: undefined});
    event.preventDefault();
  }

  dismissInfo = (event) => {
    this.setState({infoMessage: undefined});
    event.preventDefault();
  }

  handleClickCancel = () => {
    this.setState({dialogPurpose: PURPOSE_LOGIN});
  }

  handleClickRecoverPassword = (event) => {
    this.setState({dialogPurpose: PURPOSE_RECOVER_PASSWORD});
    event.preventDefault();
  }

  handleSendLink = async () => {
    this.runWithShowError(async () => {
      await authService.requestPasswordRecovery(this.state.credentials.email);
      this.setState({
        infoMessage: "The link was sent to your email address.",
        dialogPurpose: PURPOSE_LOGIN,
      });
    })
  }

  handleClickLogin = async () => {
    this.runWithShowError(async () => {
      await authService.login(this.state.credentials)
    });
  }

  handleChange = (e) => {
    this.setState(setIn(this.state, ['credentials', e.target.name], e.target.value));
  }

  renderLogin() {
    const {credentials} = this.state;
    return (
      <div className="login-page">
        <div className="login-box">

          <div className="login-logo">
            <b>Customer</b> APP
          </div>
          <div className="login-box-body overlay-container">

            <p className="login-box-msg">Please login</p>

            <Messages {...this.state} {...this} />

            <div className="form-group has-feedback">
              <input name="email" value={credentials.email} type="email" className="form-control" placeholder="Email" onChange={this.handleChange} />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div className="form-group has-feedback">
              <input name="password" value={credentials.password} type="password" className="form-control" placeholder="Password" onChange={this.handleChange} />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div className="row">
              <div className="col-xs-4 col-xs-offset-4">
                <button className="btn btn-primary btn-block" type="submit" onClick={this.handleClickLogin}>Login</button>
              </div>
            </div>

            <div className="row">
              <div className="col-xs-12 text-center">
                <p className="login-box-footer"><a href="#/" onClick={this.handleClickRecoverPassword}>Forgot your password?</a></p>
              </div>
            </div>
          </div>
          <p className="pull-right">
              ver. {this.state.version}
          </p>
        </div>
      </div>
    )
  }

  renderRecoverPassword() {
    const {credentials} = this.state;
  
    return (
      <div className="login-page">
        <div className="login-box">
          <div className="login-logo">
            <b>Customer</b> APP
        </div>
          <div className="login-box-body overlay-container">

            <p className="login-box-msg">Enter your user name, we will deliver you a reset link.</p>

            <Messages {...this.state} {...this} />

            <div className="form-group has-feedback">
              <input name="email" value={credentials.email} type="email" className="form-control" placeholder="Email" onChange={this.handleChange} />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div className="row">
              <div className="col-xs-6">
                <button className="btn btn-block" type="submit" onClick={this.handleClickCancel}>Cancel</button>
              </div>

              <div className="col-xs-6">
                <button className="btn btn-primary btn-block" type="submit" onClick={this.handleSendLink}>OK</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    );    
  }

  render() {
    switch(this.state.dialogPurpose) {
      case PURPOSE_LOGIN:
        return this.renderLogin();
        break;
      case PURPOSE_RECOVER_PASSWORD:
        return this.renderRecoverPassword();
        break;
    }
  }
}

export default Login

