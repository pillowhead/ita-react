import React from 'react'

import usersService from './users-service'
import http from '../http';
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import classNames from 'classnames'

class UserPasswordChange extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            oldPassword: "",
            newPassword: "",
            confirmedNewPassword: "",
            loggedUser: null,
            user: null,
            newPasswordValid: false,
            formValid: false,
            successResponse: null,
            failResponse: null
        }
    }
    
    componentWillMount() {
        this.loadLoggedUser()
        this.loadUser(this.props.match.params.id)
    }

    loadUser(id) {
        usersService.getUser(id).then(res => {
            this.setState({
                user: res.data
              });
        })
        .catch(err => {
            console.log("user not found")
        })
    }

    async loadLoggedUser() {
        const res = await http.get('/user')

        this.setState({
            loggedUser: res.data
          });
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        const newPassword = this.state.newPassword
    
        this.setState({
          [name]: value
        });

        if(name === "oldPassword" || name === "newPassword") {
            this.setState({
                formValid: false
              });
        }
        else if(name === "confirmedNewPassword")
        {
            if (newPassword !== value) {
                this.setState({
                    newPasswordValid: false,
                    formValid: false
                  });
            }
            else if (newPassword === value) {
                this.setState({
                    newPasswordValid: true
                  });

                //this.handleValidPassword()
                //this.handleValidPasswordCheck()
                
                if(this.state.oldPassword) {
                    this.setState({
                        formValid: true
                      });
                }
            }
        }
    }

    handleValidPassword() {
        let validClassname = classNames('form-group', {'has-success': this.state.newPasswordValid})       
        return validClassname
    }

    handleValidPasswordCheck() {
        let checkClassname = classNames('fa', {'fa-check': this.state.newPasswordValid})      
        return checkClassname
    }

    handleSubmit(e) {
        e.preventDefault()
        let username = ""

        if(this.state.user) {
            username = this.state.user.login
        }
        else {
            username = this.state.loggedUser.username
        }

        usersService.changePassword({"username": username,"oldPassword": this.state.oldPassword, "newPassword": this.state.newPassword})
        .then(res => {
            this.setState({
                successResponse: res.statusText
              });
        })
        .catch(err => {
            this.setState({
                failResponse: err.response.data.status
              });
        }) 
    }

    render() {
        const formValid = this.state.formValid
        const successResponse = this.state.successResponse
        const failResponse = this.state.failResponse
        let validClassname = classNames('form-group', {'has-success': this.state.newPasswordValid})
        let checkClassname = classNames('fa', {'fa-check': this.state.newPasswordValid})

        return (
            <div className="content">
                <div className="col-md-6">
                    <div className="box box-primary">
                        <div className="box-header with-border">
                        <h3 className="box-title">Change password</h3>
                        </div>
                        <form onSubmit={(e) => this.handleSubmit(e)}>
                            <div className="box-body">
                                <div>
                                    <label>Current Password</label>
                                    <input name="oldPassword" type="password" className="form-control" onChange={(event) => this.handleInputChange(event)}/>
                                </div>
                                <div className="form-group">
                                    <label>New Password</label>
                                    <input name="newPassword" type="password" className="form-control" onChange={(event) => this.handleInputChange(event)}/>
                                </div>
                                <div className={validClassname}>
                                    <label>Confirm New Password <i className={checkClassname}></i></label>
                                    <input name="confirmedNewPassword" type="password" className="form-control" onChange={(event) => this.handleInputChange(event)}/>
                                </div>
                            </div>
                            {successResponse && <div className="alert alert-success alert-dismissible margin">
                                    <Link to={ROUTES.DASHBOARD} type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</Link>
                                    <h4><i className="icon fa fa-check"></i>{successResponse}</h4>
                            </div>}
                            {failResponse && <div className="alert alert-danger alert-dismissible margin" >
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i className="icon fa fa-warning"></i>{failResponse}</h4>
                            </div>}
                            <div className="box-footer">
                                <button type="submit" className="btn btn-primary" disabled={!formValid} onClick={(e) =>this.handleSubmit(e)}>Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserPasswordChange