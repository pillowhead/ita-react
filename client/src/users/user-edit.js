import React from 'react'
//import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

import UserForm from './user-form'

class UserEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
    console.log(newProps)
  }

  load(id) {
    usersService.getUser(id).then(res => {
      this.setState({
        user: res.data
      })
    })
  }

update() {
  usersService.updateUser(this.state.user).then(res => {
    this.props.history.push(ROUTES.USER_LISTING)
  })
}

handleChange(e) {
  this.setState({
    ...this.state,
    user: {
      ...this.state.user,
      [e.target.name]: e.target.value,
    }
  })
}

  render() {
    const user = this.state.user

    return (
      <div className="col-md-6">
        <div className="box">
          <div className="box-header">
            <h3 className="box-title">Edit User</h3>
          </div>
          <div className="box-body">
            {user && <UserForm user={user} handleChange={e => this.handleChange(e)}/>}
          </div>
          <div className="box-footer">
            <button className="btn btn-primary" onClick={() => this.update()}>Save</button>
          </div>
        </div>
      </div>
    )
  }
}


export default UserEdit