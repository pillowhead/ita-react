const knexLib = require('knex');
const knex = knexLib({
  client: 'sqlite3',
  connection: {
    filename: './dev.sqlite3',
  },
  useNullAsDefault: true,
});
const express = require('express');

async function log({user, operation, entity}) {
  await knex.transaction(async knex => {
    let result = await knex('audit_log_event').insert({
      created_at: new Date(),
      user,
      operation,
    });
    let insertedId = result[0];
  
    switch(operation) {
      case 'create':
      case 'update':
      case 'delete':
        await knex('audit_log_entity').insert({
          event_id: insertedId,
          entity,
        })
        break;
    }
  })
}

async function query({entity, operation}) {
  let query = (
    knex.select('*')
      .from('audit_log_event')
  );
  if(operation && ['login', 'create', 'update', 'delete'].indexOf(operation) > -1) {
    query = query.where({operation});
  }

  if(entity && ['users', 'customers', 'contracts', 'comments'].indexOf(entity) > -1) {
    query = query.where({entity});
  }   

  if(entity === 'authentication') {
    query = query.where({operation: 'login'});
  }

  query = query.leftJoin('audit_log_entity', 'audit_log_event.id', 'audit_log_entity.event_id')

  query.orderBy('created_at', 'desc');
  //console.log(query.toString())
  return query
}

function logEntityHandler(req, res, next) {
  // We do not want to wait for log operation to finish so no await/then etc...
  const user = req.session.loggedUserName || '<anonymous>';
  const {entity} = req.params;
  try {
    if(['users', 'customers', 'contracts', 'comments'].indexOf(entity) > -1) {
      switch(req.method) {
        case 'POST':
          log({user, operation: 'create', entity});
          break;
        case 'PUT':
          log({user, operation: 'update', entity});
          break;      
        case 'DELETE':
          log({user, operation: 'delete', entity});
          break;         
      }
    }
  }
  finally {
    next();
  }
}

const middleware = express.Router();
middleware.get('/audit/query', async function(req, res) {
  res.send(await query(req.query));
})
middleware.all('/:entity/:id', logEntityHandler);
middleware.all('/:entity', logEntityHandler);




module.exports = {
  middleware ,
  log,
}