
exports.up = async function(knex, Promise) {
  const sch = knex.schema;
  await sch.createTable('audit_log_event', t => {
    t.increments();
    t.timestamp('created_at');
    t.string('user');
    t.string('operation');
  })
};

exports.down = async function(knex, Promise) {
  const sch = knex.schema;
  await sch.dropTable('audit_log_event')
};
